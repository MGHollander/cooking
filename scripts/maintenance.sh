#!/usr/bin/env bash
source "$(dirname "$0")/common.sh"

# Only allow building in dev and test environments.
if [ -z "${APP_ENV}" ] || [ "${APP_ENV}" != "development" ]; then
  echo "Maintenance script execution is only allowed in development environments.";
  exit 1;
fi

# Stop on any error.
set -e

# Drush executable.
DRUSH="../vendor/bin/drush -y"

read -p "Have you performed a composer install and a deploy? (Y/n) " -n 1 -r; echo
if [[ $REPLY =~ ^[Nn]$ ]]; then
  log "\e[33mPlease do so first.\e[39m"
  exit;
fi;

log "\e[36m--------- Performing maintenance tasks ---------\e[39m"

log "Go to drupal root."
cd "$(cd -P -- "$(dirname -- "$0")" && pwd -P)/../web" || exit 1;

log "Go to docroot."
cd .. || exit 1;

log "Update core and contrib with dependencies"
composer update

log "Go to drupal root."
cd "$(cd -P -- "$(dirname -- "$0")" && pwd -P)/../web" || exit 1;

log "Run database updates"
${DRUSH} updatedb

log "Exporting configurations."
${DRUSH} config-split:export;

log "Check if Locale module is enabled."
if [[ "Enabled" == $(${DRUSH} ${ALIAS} pm:list --status=enabled --filter="name=locale" --format=list --field=status < /dev/null) ]]; then
  log "Making sure we are working with the right translation configuration"
  ${DRUSH} cset locale.settings translation.use_source remote_and_local
  ${DRUSH} cset locale.settings translation.overwrite_not_customized true

  log "Delete the current translation status of each project (will get regenerated)."
  ${DRUSH} sql:query "DELETE FROM key_value WHERE collection='locale.translation_status';"

  log "Reset the locale_file timestamp and last checked date for all projects."
  ${DRUSH} sql:query "UPDATE locale_file SET timestamp = 0;"
  ${DRUSH} sql:query "UPDATE locale_file SET last_checked = 0;"

  log "Reset the general locale last checked date."
  ${DRUSH} state:set locale.translation_last_checked 0

  log "Check for translation updates."
  ${DRUSH} locale:check

  log "Perform translation updates."
  ${DRUSH} locale:update

  log "Export configuration."
  ${DRUSH} config:export

  ${DRUSH} cr
fi

read -p "Do you want to check for deprecations? (Please do so at least once per maintenance round) (y/N) " -n 1 -r; echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
  log "Check for deprecations."
  ${DRUSH} upgrade_status:analyze --all --ignore-contrib || true
  log "\e[33mPlease fix any found usages of deprecated APIs in custom code.\e[39m"
fi;

log "\e[36m========= End of maintenance tasks =========\e[39m"
