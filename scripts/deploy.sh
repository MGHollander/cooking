#!/usr/bin/env bash
source "$(dirname "$0")/common.sh"

# Stop on any error.
set -e

DRUSH="../vendor/bin/drush --yes"
SCRIPT_NAME=$(basename "$0")

function usage() {
    log_warning "Usage:"
    echo -e "  $SCRIPT_NAME <alias> [options] [--] "
    echo -e ""
    log_warning "Arguments:"
    echo -e "\033[32m  alias             \033[0m  Drupal alias"
    echo -e ""
    log_warning "Options:"
    echo -e "\033[32m  -h, --help        \033[0m  Display this help message"
    # echo -e "\033[32m  -f, --force       \033[0m  Force the deployment no matter the alias"
    # echo -e "\033[32m  -d, --dry-run     \033[0m  Dry-run the rsync (only work on non-local aliases)"
}

# https://medium.com/@Drew_Stokes/bash-argument-parsing-54f3b81a6a8f
PARAMS=""
while (( "$#" )); do
    case "$1" in
        -h|--help)
            usage
            exit 0
            ;;
        # -f|--force)
        #     FORCE_DEPLOY=1
        #     shift 1
        #     ;;
        -d|--dry-run)
            DRY_RUN=1
            shift 1
            ;;
        --) # end argument parsing
            shift
            break
            ;;
        -*|--*=) # unsupported flags
            log_error "Error: Unsupported flag $1" >&2
            echo ""
            usage
            exit 1
            ;;
        *) # preserve positional arguments
            PARAMS="$PARAMS $1"
            shift
            ;;
    esac
done
# set positional arguments in their proper place
eval set -- "$PARAMS"

log "\033[1m--------- Start deployment ---------"

log "Go to drupal root"
cd "$(cd -P -- "$(dirname -- "$0")" && pwd -P)/../web" || exit 1;

log "Check if we were given a valid alias"
ALIAS="$1"
[ "${ALIAS}" == "@none" ] && exit 0;
if [ "${ALIAS}" == "default" ] || [ "${ALIAS}" == "@self" ] || [ "${ALIAS}" == "" ]; then
  ALIAS="";
  IS_LOCAL="true";
fi

${DRUSH} site:alias ${ALIAS} >/dev/null || exit 1;

# if [ -z "${IS_LOCAL}" ]; then
#   if [ -n "$DRY_RUN" ]; then
#     log "Execute a dry-run rsync on remote aliases"
#     # See drush/drush.yml for additional config on drush rsync command
#     yes | ${DRUSH} rsync .. ${ALIAS}:.. -- -crblPi --verbose --exclude-from="../.rsync-exclude" --delete-after --dry-run
#     exit
#   fi

#   if [ -z "$FORCE_DEPLOY" ]; then
#     read -p "Are you sure that you want to continue this deployment to production? [y/n] " -n 1 -r
#     echo # move to a new line
#     if [[ ! $REPLY =~ ^[Yy]$ ]]; then
#       log_error "Deployment aborted"
#       exit 2
#     else
#       log_success "Continue deployment"
#     fi
#   else
#    log_warning "Force deployment"
#   fi
# else
#   echo "This is a local alias. Continue..."
# fi

log "Enable maintenance mode"
${DRUSH} ${ALIAS} state:set system.maintenance_mode 1

# if [ -z "${IS_LOCAL}" ]; then
#   log "Execute rsync on remote aliases (no deletions)"
#   # We don't do deletions here because uninstalling modules might require the module code.
#   # See drush/drush.yml for additional config on drush rsync command
#   yes | ${DRUSH} rsync .. ${ALIAS}:.. -- -crblPi --verbose --exclude-from="../.rsync-exclude"

#   log_success "Successfully finished rsync"
# else
#   log_warning "Skip rsync on local aliases"
# fi

log "Creating db backup"
if [ "${ALIAS}" == "@acc" ] || [ "${ALIAS}" == "@prod" ]; then
  ${DRUSH} ${ALIAS} sql:dump --gzip --result-file=auto || exit 1

  log_success "Successfully finished db backup"
else
  log_warning "Skipped, db backup is for acceptance and production deployments only"
fi

log "Perform drupal database updates."
${DRUSH} ${ALIAS} updatedb

# if [ -z "${IS_LOCAL}" ]; then
#   log "Rsync configuration deletions to remote aliases."
#   # Still no full deletion, since uninstalling a module (which might happen on configuration import) may require the module code.
#   # See drush/drush.yml for additional config on drush rsync command
#   yes | ${DRUSH} rsync ../config ${ALIAS}:.. -- -crblPi --verbose --delete-after

#   log_success "Successfully finished rsync"
# else
#   log_warning "Skip rsync configuration deletions on local aliases."
# fi

log "Check if locale module is enabled."
if [[ "Enabled" == $(${DRUSH} ${ALIAS} pm:list --status=enabled --filter="name=locale" --format=list --field=status < /dev/null) ]]; then
  echo "Temporarily set translation imports to local only. On development environments,"
  echo "  this will be reverted back to remote_and_local when importing config."
  ${DRUSH} ${ALIAS} config:set locale.settings translation.use_source local

  log "Check for translation updates"
  ${DRUSH} ${ALIAS} locale:check

  log "Perform translation updates"
  ${DRUSH} ${ALIAS} locale:update

  log_success "Successfully updated translations"
else
  log_warning "Locale module is not enabled"
fi

log "Import the configuration"
${DRUSH} ${ALIAS} config:import

# if [ -z "${IS_LOCAL}" ]; then
#   log "Finish rsync to remote aliases (with deletions)"
#   # See drush/drush.yml for additional config on drush rsync command
#   yes | ${DRUSH} rsync .. ${ALIAS}:.. -- -crblPi --verbose --exclude-from="../.rsync-exclude" --delete-after

#   log_success "Successfully finished rsync"
# else
#   log_warning "Skip rsync deletions on local aliases"
# fi

log "Clear all caches"
${DRUSH} ${ALIAS} cache:rebuild

# log "Check if default content module is enabled."
# if [[ "Enabled" == $(${DRUSH} ${ALIAS} pm:list --status=enabled --filter="name=default_content" --format=list --field=status < /dev/null) ]]; then
#   log "Install any default content."
#   # Import all does not work at the moment...
#   ${DRUSH} ${ALIAS} default-content:import-all < /dev/null
# else
#   log "\e[33mDefault content module is not enabled.\e[39m"
# fi
#
# log "Clear all caches"
# ${DRUSH} ${ALIAS} cache:rebuild

log "Exit maintenance mode"
${DRUSH} ${ALIAS} state:set system.maintenance_mode 0

log "\033[1m========= End of deployment ========="
