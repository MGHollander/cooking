#!/usr/bin/env bash
source "$(dirname "$0")/common.sh"

log "\033[1m--------- Start build ---------"

log "Installing drupal composer dependencies."
composer install --no-interaction --ignore-platform-reqs --no-progress "$@" || exit 1;

# Composer install a second time. See https://github.com/cweagans/composer-patches/issues/253
log "Composer install a second time."
composer install --no-interaction --ignore-platform-reqs --no-progress "$@" || exit 1;

#if hash npm; then
#  cd web/themes/custom/koken || exit 2
#  npm run build
#fi

log "\033[1m========= End of build ========="
