#!/usr/bin/env bash
source "$(dirname "$0")/common.sh"

# Only allow clean install in dev and test environments.
if [ -z "${APP_ENV}" ] || ([ "${APP_ENV}" != "development" ] && [ "${APP_ENV}" != "test" ]); then
  echo "Clean install is only allowed in development and test environments.";
  exit 1;
fi

log "\033[1m--------- Performing clean install ---------"

read -p "A clean install will delete the current database and empty the files folder. Are you sure that you want to continue? [y/N] " -n 1 -r
echo # move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
  log_error "Clean install aborted"
  exit 2
else
  log_success "Continue clean install"
fi

log "Go to the project root"
cd "$(cd -P -- "$(dirname -- "$0")" && pwd -P)/.." || exit 1

log "Clean up drupal files dir"
rm -rf web/sites/default/files/*

log "Run build script"
bash scripts/build.sh || exit 1

log "Go to the drupal root"
cd web || exit 1;

log "Install drupal"
drush site:install minimal --locale=nl --config-dir=../config/sync --yes --account-name=root --account-pass=supersecret || exit 1

log "Go to the project root"
cd ..

log "Run deploy script"
bash scripts/deploy.sh || exit 1

log "\033[1m========= End of clean install ========="
