#!/usr/bin/env bash
source "$(dirname "$0")/common.sh"

# Only allow sanitize in dev and test environments.
if [ -z "${APP_ENV}" ] || [ "${APP_ENV}" != "development" ]; then
  echo "Sanitize script execution is only allowed in development environments.";
  exit 1;
fi

# Stop on any error.
set -e

# Drush executable.
DRUSH="../vendor/bin/drush -y"

log "Go to drupal root."
cd "$(cd -P -- "$(dirname -- "$0")" && pwd -P)/.." || exit 1;

log "Run sanitization operations on the current database."
${DRUSH} sql-sanitize --sanitize-email=%uid@localhost.test;
# Other required database sanitization should go here.
# You might want to import configuration (splits) for the development environment.

log "Perform drupal database updates."
${DRUSH} updatedb -y

log "Clear all caches."
${DRUSH} cache-rebuild
