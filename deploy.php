<?php

namespace Deployer;

require 'vendor/autoload.php';

use Deployer\Exception\Exception;
use phpseclib3\Net\SFTP;

require 'recipe/drupal8.php';

set('application', 'koken.maruc.nl');
set('default_stage', 'production');
set('http_user', 'koken');
set('local_deploy_path', 'tmp_deploy');
set('repository', 'git@gitlab.com:MGHollander/cooking.git');
set('shared_dirs', [
  'web/sites/{{drupal_site}}/files',
]);
set('shared_files', [
  'web/sites/{{drupal_site}}/settings.php',
  'web/sites/{{drupal_site}}/settings.local.php',
]);
set('writable_dirs', [
  'web/sites/{{drupal_site}}/files',
]);
set('writable_mode', 'chmod');

host('koken.maruc.nl')
  ->stage('production')
  ->set('deploy_path', '/home/koken/domains/koken.maruc.nl');

desc('Prepares a new release');
task('deploy:update_code', function () {
  $tag = input()->getOption('tag');
  if ($tag) {
    set('branch', $tag);
  }

  if (testLocally('[ -d {{ local_deploy_path }} ]')) {
    writeLn('Found a local deploy dir. Remove it!');
    runLocally('rm -rf {{ local_deploy_path }} ');
  }

  if (!testLocally('git show-ref {{ branch }} >/dev/null 2>&1')) {
    throw new Exception('No Git reference to ' . get('branch') . ' found.');
  }

  writeLn('Clone <info>{{ branch }}</info> into <info>{{ local_deploy_path }}</info>');
  runLocally('git clone {{ repository }} {{ local_deploy_path }} --branch {{ branch }} --single-branch --depth=1');

  writeLn('Remove files that should not be uploaded');
  runLocally('rm -rf {{ local_deploy_path }}/{.git,.rsync-exclude,.editorconfig,.gitattributes,.gitignore,.gitlab-ci.yml}');
  runLocally('rm -rf {{ local_deploy_path }}//web/themes/custom/umami/src}');
  runLocally('find {{ local_deploy_path }} -type f -name "README.*" -delete');

  writeLn('Upload files to <info>{{ release_path }}</info>');
  runLocally('scp -rC {{ local_deploy_path }}/* {{ hostname }}:{{ release_path }}');

  runLocally('rm -rf {{ local_deploy_path }}');
});

task('deploy:drupal_deploy', function () {
  runLocally('bash scripts/deploy.sh @prod', ['tty' => TRUE]);
});

after('deploy:update_code', 'deploy:vendors');
after('deploy:vendors', 'deploy:drupal_deploy');
after('deploy:failed', 'deploy:unlock');
