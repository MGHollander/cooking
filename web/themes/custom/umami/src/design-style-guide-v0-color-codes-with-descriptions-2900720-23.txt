These are the colour codes for the Umami themse, from the Design Styleguide v0.
See "Designs for the Out of the Box experience intiative", comment #21
  - https://www.drupal.org/node/2900720#comment-12259232

They colours are in order of first mention in the style guide v0, together with
the description used.  Some colours are re-used with multiple descriptions.

#000000
  Text base

#00836D
  VIEW RECIPE >

#ffffff
  Text

#E84265
  Button
  Numbers

#5F635D
  Background grey
  Heading prefixes/labels
  Labels

#FBF5EE
  Peach tint for background:

#FF6138
  Underscore

#00836D
  Term link

#79BD8F
  Decorative separator

#F4F2E9
  Quotation BG
  BG

#787C75
  Term Link

#464646
  Text

#EEC2CB
  Border
