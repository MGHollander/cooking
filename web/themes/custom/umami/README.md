# Umami

Umami is the theme used for the "Umami food magazine" demonstration site. The
Umami theme is a part of the ['Out of the Box'](https://www.drupal.org/node/2847582)
initiative for Drupal. The Umami theme uses Classy as its base theme.

**Note:** The Umami theme files from the profile have been added to this project
manually, because it's not add advised to use the profile for production websites

## Designs
Designs are attached to the summary of <a href="https://www.drupal.org/node/2900720">
\#2900720: Designs for the Out of the Box experience initiative</a> and are also
available in <a href="https://projects.invisionapp.com/share/MECDJD8GP#/screens/243951129_Umami_-_Front_-_Sketch_1_-_Desktop">
InVision</a>. A visual and textual style guide is available in the src directory.

The logo uses the Kewl Script Regular font, which is available in the src/fonts
folder. The subtitle uses Avenir Next. The color of the logo is `#da3c13`. The
GIMP (https://www.gimp.org/) source file for the logo is available in the
src/logo folder.

## Resources
* There is a <a href="https://drive.google.com/drive/folders/0B7MA3IYYh44bMzNsVXhKNGpZNDQ">
  Google Drive folder</a> with shared files (documents, assets etc.).
* Here is a PDF style guide with colors, typography, etc. The latest version can
  be found in the <a href="https://www.drupal.org/node/2881910#comment-12279271">issue</a>.

## Standard css

### Breakpoints

**Formatting**

The CSS formatting guidlines require that breakpoints are as follows

> Media queries should be written in the same style as ruleset. Any containing
> rulesets are indented by two spaces.

> * One space between the media feature and the value.
> * All values to be written in rems unless it is inappropriate.
> * Add the pixel value in a comment directly after the the opening brace.

```
@media screen and (min-width: XXrem) { /* YYYpx */
  .selector {
    /* Styles */
  }
}
```

**Positioning**

Place the media queries immediately after the selector to which they relate

**Sizes**

The following small, medium, large and extra large sizes can be ordinarily
followed.

If an element needs a custom breakpoint use it.

```
/* Small */
@media screen and (min-width: 30rem) { /* 480px */

}
/* Medium */
@media screen and (min-width: 48rem) { /* 768px */

}
/* Large */
@media screen and (min-width: 60rem) { /* 960px */

}
/* Extra large */
@media screen and (min-width: 75rem) { /* 1200px */

}
```
