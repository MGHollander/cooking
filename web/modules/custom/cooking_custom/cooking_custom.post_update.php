<?php

/**
 * @file
 * Update post file for the cooking_custom module.
 */

use Drupal\paragraphs\Entity\Paragraph;

/**
 * Move ingredients field to ingredients list paragraph.
 */
function cooking_custom_post_update_9201(&$sandbox) {
  $storage = \Drupal::entityTypeManager()->getStorage('node');

  // Initialize some variables during the first pass through.
  if (!isset($sandbox['total'])) {
    $query = $storage->getQuery()
      ->condition('type', 'recipe')
      ->accessCheck(FALSE);

    $nids = $query->execute();

    $sandbox['total'] = count($nids);
    $sandbox['ids'] = array_chunk($nids, 1);
    $sandbox['current'] = 0;
  }

  if ($sandbox['total'] == 0) {
    $sandbox['#finished'] = 1;
    return;
  }

  $nids = array_shift($sandbox['ids']);
  $nodes = $storage->loadMultiple($nids);

  /* @var $node \Drupal\node\NodeInterface $node */
  foreach ($nodes as $node) {
    $sandbox['current']++;

    if (!$node->hasField('field_ingredients') || !$node->hasField('field_ingredients_list')) {
      \Drupal::logger('cooking_custom')->notice('Skip ' . $node->get('title')->getString());
      continue;
    }

    $paragraph = Paragraph::create([
      'type' => 'paragraph_ingredients_list',
      'field_ingredients' => $node->get('field_ingredients')->getValue(),
    ]);
    $paragraph->save();

    $current = $node->get('field_ingredients_list')->getValue();
    $current[] = [
      'target_id' => $paragraph->id(),
      'target_revision_id' => $paragraph->getRevisionId(),
    ];

    $node->set('field_ingredients_list', $current);
    $node->save();

    \Drupal::logger('cooking_custom')->notice('Copied ' . $sandbox['current'] . ' of ' . $sandbox['total'] . ' ingredient lists.');
  }

  $sandbox['#finished'] = min(($sandbox['current'] / $sandbox['total']), 1);
}
