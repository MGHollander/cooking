# Koken met Marc

This is the project repository for Koken met Marc.

## Prerequisites

1.  `APP_ENV` environment variable set (presumably to `development`); this
    is not required for production environments, as it is the default. This
    will depend on your development environment.

## Project setup

1.  Clone the repository.
2.  Copy `web/sites/default/default.settings.php` and rename to `settings.php`.
3.  Create a database and add the database credentials to `settings.php`.
    ```php
    $databases['default']['default'] = array (
      'database' => '',
      'username' => '',
      'password' => '',
      'prefix' => '',
      'host' => 'localhost',
      'port' => '3306',
      'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
      'driver' => 'mysql',
    );
    ```
4.  Copy `web/sites/example.settings.local.php` to `web/sites/default/` and
    rename it to `settings.local.php`.
5.  Open `settings.local.php` and change
    `$config['config_split.config_split.development']['status']` to `TRUE`.
6.  Run `scripts/clean-install.sh` (you may need to prepend with e.g. `bash`).
    This will create a copy of the website including demo content.

## Scripts

Some helper scripts are located in the `scripts` directory.

`build.sh`

Is for running composer installs for the site and theme and to perform any other
build tasks.

`deploy.sh`

Is for deploying the site, either locally or remotely. It is meant to be run
after each installation or deployment of the codebase. Runs tasks like database
updates, importing translations, applying configuration, importing default
content, reverting features, etc. If given a remote alias it can also transfer
over the current codebase.

**Save deployment output to file**

ZSH: `bash scripts/deploy.sh @alias >&1 > ../log-$(date +'%Y%m%d%H%M%S').txt 2>&1`
Bash: `bash scripts/deploy.sh @alias 2>&1 | tee ../log-$(date +'%Y%m%d%H%M%S').txt`

`clean-install.sh`

Is for (re-)installing the site with an empty database and files. It calls the
build and deploy scripts and performs an install in between.

`maintenance.sh`

Is for installing drupal core and contrib module updates. It uses Composer to
search for updates.

`sanitize.sh`

Is to remove privacy sensitive data from copies of the production database.

## Configuration management

This site comes with config-split included. This means that you can have
configuration that is different between environments.

Environment specific settings.local.php overrides are available in
`/web/sites/default`. These are read from settings.local.php (as long as you use
the provided example.settings.local.php as a source). Here you can store
migration backends, cache settings and other non-secret configuration that needs
to differ between environments.

**Do NOT store passwords in your git repo.** Those have to go in the actual
settings.php on the specific environment.

To update the configuration you can edit any and all settings you like in the
installed site. Then go to the web dir and execute `drush cex` to create the
new settings on the filesystem (in the sync directory).

## Development

This project is using continues intergration (CI) offered by GitLab. The
project is only using a main branch. New features need to be branched from the
main branch. Merge requests are used to merge code to the main branch. Every
push will trigger the build job in GitLab. Merge requests cannot be merged
without a succeeded build job.

## Deployment

Once you are satisfied with the changes in main, and you want to deploy to
production, then you have to create a new release via
<https://gitlab.com/MGHollander/cooking/-/releases> ot via Git.
The project uses [semantic versioning](https://semver.org/). Check what the
latest release tag was and add one version in the new tag. Use something like
"Production release vx.x.x" as tag message and add the resolved issues in the
release notes field if it's a public release.

After the tag has been created you have to run the deploy script from the
project root in your terminal.

```
vendor/bin/dep deploy --tag <tag>
```

You can also deploy a branch.

```
vendor/bin/dep deploy --branch <branch>
```

Deployer is used for the deployments. See <https://deployer.org/docs/6.x/cli>
for more about the deployment script.
